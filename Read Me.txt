 ______ _____ _     _   _ _   _  _____                  _   
|___  /|  ___| |   | | | | \ | ||  ___|                | |  
   / / | |__ | |   | | | |  \| || |__        _ __   ___| |_ 
  / /  |  __|| |   | | | | . ` ||  __|      | '_ \ / _ \ __|
./ /___| |___| |___| |_| | |\  || |___   _  | | | |  __/ |_ 
\_____/\____/\_____/\___/\_| \_/\____/  (_) |_| |_|\___|\__|

Thank you for downloading Zelune Proxy Script v2.1 from Zelune.net




SUPPORT/CONTACT:
Collin Raithe [Raithe@Gmail.com]



INSTALLATION:

CHMOD "cookies" to 777
CHMOD "uploads" to 777
Ensure safe_mode is off on your server
Ensure open_basedir is UNSET (PHP Related, php.ini).
Ensure cURL is installed on your server.  (http://curl.haxx.se is the homepage).



CHANGELOG:

Zelune v2.1 [June 3rd]
- Minor code fixes for optimization
- Released free Zelune Proxy Themes at http://zelune.net/zelune-proxy-themes/
- Released zelune proxy list at http://zelune.net/zelune-proxy-list/
- Zelune proxy blog (Zelune.net/blog) coming soon.

Zelune v2 [May 28th 2007]
- Changed proxy url from '__proxy_url' to '__new_url' to avoid auto-proxy network detection [By popular request]


Zelune v1 [May 26-2007]
- Created Zelune.net.
- Released Zelune source code to the public.


CREDITS:
Zelune & Template From:
Zelune.net

Script author:
Antonio Giorlando

Zelune.net owner:
Collin Raithe
raithe@gmail.com

